from safemode_commons.web.clients.iamclient import IamClient
from safemode_commons.web.clients.martclient import MartClient
from safemode_data_tenant_transdev.configuration import config

iam_client = IamClient(config=config)
iam_client.auth.client_login()

mart_client = MartClient(config=config)
