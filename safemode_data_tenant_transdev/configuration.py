import logging.config
import os

from safemode_commons.web.webclientconfiguration import WebClientConfiguration

try:
    logging.config.fileConfig(fname='safemode_data_tenant_transdev/config/logger.cnf')
    logging.getLogger().setLevel(level=logging.INFO)
    logging.getLogger('webrequestdispatcher').setLevel(level=logging.ERROR)
except Exception as e:
    print(f"An error occurred while configuring logger: {e}")

APPLICATION_NAME = os.environ.get('APPLICATION_NAME')
APPLICATION_PORT = os.environ.get('APPLICATION_PORT')
ACTIVE_PROFILE = os.environ.get('ACTIVE_PROFILE')
CLOUD_CONFIG_URI = os.environ.get('CLOUD_CONFIG_URI')
CLOUD_CONFIG_USERNAME = os.environ.get('CLOUD_CONFIG_USERNAME')
CLOUD_CONFIG_PASSWORD = os.environ.get('CLOUD_CONFIG_PASSWORD')
CLOUD_CONFIG_LABEL = os.environ.get('CLOUD_CONFIG_LABEL')

APP_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'safemode_data_tenant_transdev')


config = WebClientConfiguration(
    appname=APPLICATION_NAME,
    profile=ACTIVE_PROFILE,
    config_server=CLOUD_CONFIG_URI,
    config_username=CLOUD_CONFIG_USERNAME,
    config_password=CLOUD_CONFIG_PASSWORD,
    config_label=CLOUD_CONFIG_LABEL
)