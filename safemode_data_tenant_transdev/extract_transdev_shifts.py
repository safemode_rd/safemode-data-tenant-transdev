import logging

import time

from safemode_data_tenant_transdev.aws_clients import s3_client, metrics
from safemode_data_tenant_transdev.shift_repository import persists_shifts
from safemode_data_tenant_transdev.environment import environment

TRANSDEV_SHIFTS_S3_BUCKET = 'safemode-ftp'
TRANSDEV_SHIFTS_S3_FOLDER = 'transdev'
SHIFTS_BATCH = 500
TOKEN_KEY = f'{TRANSDEV_SHIFTS_S3_FOLDER}/extraction_token'
logger = logging.getLogger('shift_extractor')


def get_extraction_token():
    results = s3_client.list_objects_v2(Bucket=TRANSDEV_SHIFTS_S3_BUCKET, Prefix=TOKEN_KEY)
    if 'Contents' in results:
        return s3_client.get_object(Bucket=TRANSDEV_SHIFTS_S3_BUCKET, Key=TOKEN_KEY)['Body'].read().decode()
    return None


def update_shift_token(new_shift_token):
    logger.info(f'updating s3 shift token to {new_shift_token}')
    s3_client.put_object(Bucket=TRANSDEV_SHIFTS_S3_BUCKET, Key=TOKEN_KEY, Body=new_shift_token.encode())


def extract_transdev_shifts(transdev_tenant_pid):
    try:
        logger.info(f'Starting transdev shifts extraction, batch size is {SHIFTS_BATCH}')
        start_time = time.time()
        shift_token = get_extraction_token()
        operation_parameters = {'Bucket': TRANSDEV_SHIFTS_S3_BUCKET,
                                'Prefix': TRANSDEV_SHIFTS_S3_FOLDER,
                                'MaxKeys': SHIFTS_BATCH}
        if shift_token is not None:
            logger.info(f'current shift token is {shift_token}')
            operation_parameters['StartAfter'] = shift_token
        objects = s3_client.list_objects_v2(**operation_parameters).get('Contents')
        if objects is not None:
            csv_files = []
            for csv_object in objects:
                csv_file = s3_client.get_object(Bucket=TRANSDEV_SHIFTS_S3_BUCKET, Key=csv_object['Key'])
                csv_files.append(csv_file['Body'].read())
            persists_shifts(transdev_tenant_pid, csv_files)
            shift_token = objects[-1]['Key']
            update_shift_token(shift_token)
            extraction_time = time.time() - start_time
            logger.info(f'shifts extraction took {extraction_time} seconds')
        else:
            logger.info('No new shifts')
    except Exception:
        metrics.put_metric_data(MetricData=[
            {
                'MetricName': 'shifts_extraction_failure',
                'Dimensions': [
                    {
                        'Name': 'environment',
                        'Value': environment
                    }
                ],
                'Unit': 'Count',
                'Value': 1
            },
        ],
            Namespace='transdev-shifts')
        logger.exception(
            f"Shifts update failed")
