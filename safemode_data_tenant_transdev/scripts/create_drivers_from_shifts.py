import io

import boto3
import pandas as pd


# Create a client
client = boto3.client('s3', region_name='us-west-2')

# Create a reusable Paginator
paginator = client.get_paginator('list_objects')

# Create a PageIterator from the Paginator
page_iterator = paginator.paginate(Bucket='safemode-ftp', Prefix='transdev')
final_df = pd.DataFrame()
for page in page_iterator:
    shift_dfs = [final_df]
    if page['Contents']:
        for item in page['Contents']:
            csv_file = client.get_object(Bucket='safemode-ftp', Key=item['Key'])
            shift_dfs.append(pd.read_csv(io.BytesIO(csv_file['Body'].read()), dtype=str))
    final_df = pd.concat(shift_dfs)[['DriverID', 'Firstname', 'Lastname']].drop_duplicates()
final_df.to_parquet('drivers.parquet', compression='gzip')
