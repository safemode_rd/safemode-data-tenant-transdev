import pandas as pd
from safemode_data_tenant_transdev.safemode_clients import mart_client
import requests


shifts_df = pd.read_parquet('drivers.parquet')

print(f'number of drivers : {len(shifts_df.index)}')

transdev_tenant = '786b5bc0-8726-4aa6-a36a-61deb9925f4a'

lytx_vendor_example = {'type': 'lytx'}
lytx_vendor = mart_client.vendors.find_one_by_example(example=lytx_vendor_example,
                                                      tenant_pid=f'tenant:fleet:{transdev_tenant}')
lytx_vendor_id = lytx_vendor['id']

s = requests.Session()

vhecile_creation_url = f'https://dev.api-safemode.co/admin/v1/fleets/{transdev_tenant}/drivers'

s.post(url='https://dev.api-safemode.co/admin/v1/auth/password_login',
       json={"username": "marcelo@safemode.co",
             "password": "marcelo123!@#"})

for row in shifts_df.itertuples():
    user_id = row.DriverID
    first_name = row.Firstname
    last_name = row.Lastname

    safemode_driver = {
        "enabled": True,
        "firstname": first_name,
        "lastname": last_name,
        "locale": 'us_EN',
        'contact_info': {},
        'nbf': "2020-01-01",
        'zone_id': "US/Central",
        "vendors_aliases": {
            'employee_id': str(user_id),
        },
        "vendors_ids": [
            lytx_vendor_id
        ]
    }
    try:
        response = s.post(url=vhecile_creation_url, json=safemode_driver)
        if response.status_code == 200:
            print(f"created: {safemode_driver}, id: {response.text}")
        else:
            print(f'{safemode_driver} update failed, response: {response.json()}')
    except Exception as e:
        print(f'{safemode_driver} update failed, error: {e}')
