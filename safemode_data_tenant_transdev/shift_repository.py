import io
import logging
from functools import lru_cache

import time

from safemode_data_tenant_transdev.aws_clients import metrics, s3_client
import pandas as pd
from datetime import datetime, timedelta
from safemode_commons.utils import pid
from safemode_data_tenant_transdev.environment import environment


from safemode_data_tenant_transdev.safemode_clients import mart_client

SHIFT_PARQUETS_BUCKET = 'safemode-papyrus'

TRANSDEV_DATE_FORMAT = '%m/%d/%Y %H:%M:%S'

TRANSDEV_DAY_FORMAT = '%m/%d/%Y'

SHIFT_VERSION = 1

logger = logging.getLogger('shift_repository')


def convert_to_datetime(date_string, time_string, tz_offset):
    date_time_str = date_string + " " + time_string
    try:
        datetime_obj = datetime.strptime(date_time_str, TRANSDEV_DATE_FORMAT)
        if tz_offset:
            delta = timedelta(hours=int(tz_offset))
            datetime_obj = datetime_obj - delta
        return datetime_obj
    except Exception:
        logger.exception(
            f"Failed to parse shift start time: {date_time_str} tz_offset: {tz_offset}")
        return None


def day_datetime(day_string):
    return datetime.strptime(day_string, TRANSDEV_DAY_FORMAT)


def get_shift_s3_file(tenant_id, start_day):
    return f"shifts" \
           f"/fleet_pid={tenant_id}" \
           f"/version={SHIFT_VERSION}" \
           f"/year={start_day.year}" \
           f"/month={start_day.month:02d}" \
           f"/{start_day.day:02d}.parquet.gzip"


def get_shift_s3_path(tenant_id, start_day):
    return f"s3://{SHIFT_PARQUETS_BUCKET}" \
           f"/{get_shift_s3_file(tenant_id, start_day)}"


def file_exist(key):
    results = s3_client.list_objects_v2(Bucket=SHIFT_PARQUETS_BUCKET, Prefix=key)
    return 'Contents' in results


def get_existing_day(tenant_id, start_day):
    if file_exist(get_shift_s3_file(tenant_id, start_day)):
        s3_path = get_shift_s3_path(tenant_id, start_day)
        return pd.read_parquet(s3_path)
    return None


def persist_shifts(df, tenant_id, start_day):
    df.to_parquet(get_shift_s3_path(tenant_id, start_day), compression='gzip')


@lru_cache(maxsize=6000)
def fetch_alias(alias_type_id, alias_name, tenant_pid):
    try:
        start_time = time.time()
        vehicle = mart_client.members.find_by_alias(
            alias_type_id=alias_type_id,
            alias_name=alias_name,
            tenant_pid=tenant_pid)
        fetch_vehicle_latency = time.time() - start_time
        metrics.put_metric_data(MetricData=[
            {
                'MetricName': 'fetch_alias_latency',
                'Dimensions': [
                    {
                        'Name': 'environment',
                        'Value': environment
                    }
                ],
                'Unit': 'Seconds',
                'Value': fetch_vehicle_latency
            },
        ],
            Namespace='transdev-shifts')
        logger.debug(f'Lytx vehicle id fetch latency is {fetch_vehicle_latency}')
        if vehicle:
            return vehicle['id']
    except Exception:
        logger.exception(
            f"Alias fetch failed for fleet: {tenant_pid}  alias_type_id: {alias_type_id} alias_value: {alias_name}")
    return None


def report_matches(entity_type, data_frame, column_name):
    total_rows = len(data_frame.index)
    matches = len(data_frame[data_frame[column_name].notnull()].index)
    misses = total_rows - matches
    matches_percent = matches / total_rows * 100
    logger.info(f'{entity_type} - matches : {matches}, misses : {misses}, matches percent: {matches_percent}')
    metrics.put_metric_data(MetricData=[
        {
            'MetricName': 'shift_matches',
            'Dimensions': [
                {
                    'Name': 'entity_type',
                    'Value': entity_type
                },
                {
                    'Name': 'environment',
                    'Value': environment
                }
            ],
            'Unit': 'Count',
            'Value': matches
        },
    ],
        Namespace='transdev-shifts')
    metrics.put_metric_data(MetricData=[
        {
            'MetricName': 'shift_misses',
            'Dimensions': [
                {
                    'Name': 'entity_type',
                    'Value': entity_type
                },
                {
                    'Name': 'environment',
                    'Value': environment
                }
            ],
            'Unit': 'Count',
            'Value': misses
        },
    ],
        Namespace='transdev-shifts')
    metrics.put_metric_data(MetricData=[
        {
            'MetricName': 'shift_matches_percent',
            'Dimensions': [
                {
                    'Name': 'entity_type',
                    'Value': entity_type
                },
                {
                    'Name': 'environment',
                    'Value': environment
                }
            ],
            'Unit': 'Percent',
            'Value': matches_percent
        },
    ],
        Namespace='transdev-shifts')


def persists_shifts(tenant_pid, csv_files):
    shift_dfs = []
    for csv_file in csv_files:
        shift_dfs.append(pd.read_csv(io.BytesIO(csv_file), dtype=str))

    shifts_df = pd.concat(shift_dfs)

    alias_type_id = mart_client.aliases_types.find_by_name(name='vehicle_name', tenant_pid=tenant_pid)['id']
    shifts_df['safemode_vehicle_id'] = shifts_df.apply(
        lambda row: fetch_alias(alias_type_id, row.VehicleID, tenant_pid), axis=1)
    report_matches("vehicle", shifts_df, 'safemode_vehicle_id')

    alias_type_id = mart_client.aliases_types.find_by_name(name='employee_id', tenant_pid=tenant_pid)['id']
    shifts_df['safemode_driver_id'] = shifts_df.apply(
        lambda row: fetch_alias(alias_type_id, row.DriverID, tenant_pid), axis=1)
    report_matches("driver", shifts_df, 'safemode_driver_id')

    shifts_df = shifts_df[shifts_df.safemode_driver_id.notnull() & shifts_df.safemode_vehicle_id.notnull()].copy()

    if shifts_df.empty:
        return

    # start time in utc
    shifts_df['start_time_utc'] = shifts_df.apply(
        lambda row: convert_to_datetime(row.ShiftStartDate, row.ShiftStartTime, row.TZ),
        axis=1)
    shifts_df = shifts_df[shifts_df.start_time_utc.notnull()].copy()

    if shifts_df.empty:
        return

    # end time in utc
    shifts_df['end_time_utc'] = shifts_df.apply(lambda row: convert_to_datetime(row.ShiftEndDate, row.ShiftEndTime, row.TZ),
                                            axis=1)
    shifts_df = shifts_df[shifts_df.end_time_utc.notnull()].copy()

    if shifts_df.empty:
        return

    # we need the start day in utc
    shifts_df['start_day'] = shifts_df.apply(lambda row: row.start_time_utc.strftime(TRANSDEV_DAY_FORMAT), axis=1)

    grouped_by_shift_day = shifts_df.groupby(shifts_df.start_day)
    tenant_id = pid.id_by_name(tenant_pid, 'tenant')
    for shift_day, day_df in grouped_by_shift_day:
        logger.info(f"Handling day: {shift_day}")
        day_df = day_df.drop(['start_day', 'ShiftStartTime', 'ShiftStartDate', 'ShiftEndDate', 'ShiftEndTime'], axis=1)
        datetime_day = day_datetime(shift_day)
        current_day_df = get_existing_day(tenant_id, datetime_day)
        if current_day_df is not None:
            logger.info(f"File already exists for: {shift_day}")
            day_df = pd.concat([current_day_df, day_df])
        final_day_df = day_df.drop_duplicates()
        persist_shifts(final_day_df, tenant_id, datetime_day)
