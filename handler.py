try:
    import unzip_requirements
except ImportError:
    pass

import logging
from safemode_data_tenant_transdev.extract_transdev_shifts import extract_transdev_shifts


logger = logging.getLogger('handler')


def handle_transdev_shifts(event, context):
    logger.info(f"Received message: {event}")
    tenant_pid = event.get("tenant_pid")
    if tenant_pid is None:
        logger.error("Transdev tenant pid must be supplied")
    extract_transdev_shifts(tenant_pid)
